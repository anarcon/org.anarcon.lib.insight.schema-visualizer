import org.anarcon.mx1.lib.insight.umlVisualizer.dialogs.GenericDialog;
import org.anarcon.mx1.lib.insight.umlVisualizer.dialogs.GenericMessage;
import org.anarcon.mx1.lib.insight.umlVisualizer.parser.InsightCaller;
import org.anarcon.mx1.lib.insight.umlVisualizer.parser.InsightUmlRepository;
import org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates.InsightObjectType;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.renderer.DotBuilder;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlEdge;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlNode;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {


    private JTextField baseUrl;
    private JButton startButton;
    private JPanel mainWindow;
    private JTextField username;
    private JPasswordField password;

    /**
     * Create Action Listeners
     */
    private Main() {
        startButton.addActionListener(e -> handleStartOperation());
        password.addActionListener(e -> handleStartOperation());
        baseUrl.addActionListener(e -> handleStartOperation());
        username.addActionListener(e -> handleStartOperation());
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Main");
        frame.setContentPane(new Main().mainWindow);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Starts actual execution of the programs logic.
     */
    private void handleStartOperation(){
        if(validateUrl(baseUrl.getText())) {
            formatUrl();
            InsightCaller caller = new InsightCaller(baseUrl.getText(), username.getText(), password.getPassword());
            InsightUmlRepository insRepo = new InsightUmlRepository(caller);
            int schema = insRepo.getSchemaId();
            System.out.println("Hello");
            if (schema >= 0) {
                List<InsightObjectType> insightObjectTypes = insRepo.collectObjectTypes(schema);
                insRepo.createUmlShapes(insightObjectTypes);
                //creating a node HashMap
                Map<Integer, UmlNode> nodes = new HashMap<>();
                insRepo.getNodes().forEach(n-> nodes.put(n.getId(),n));
                //creating a list of all the edges
                List<UmlEdge> edges = new ArrayList<>();
                edges.addAll(insRepo.getAttributeEdges());
                edges.addAll(insRepo.getParentEdges());
                edges.addAll(insRepo.getInheritanceEdges());
                //creates the .dot
                DotBuilder dotBuilder = new DotBuilder(nodes,edges);
                try {
                    dotBuilder.dotGraph();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else {
                GenericMessage warning = new GenericMessage("No schemas found in instance!", "Oops");
                warning.showDialog();
            }
        }
    }

    /**
     * Validates the given input is actually a URL. If it isn't, user can still choose to ignore the warning
     * @param url the input from field baseUrl
     * @return if it is a URL
     */
    private boolean validateUrl(String url){
        try{
            new URL(url).toURI();
            return true;
        }
        catch (Exception e){
            GenericDialog choice = new GenericDialog("Invalid URL detected, continue anyway?", "Warning");
            return choice.showDialog();
        }
    }

    /**
     * If URL ends with "/" it's removed, so all the REST-calls work correctly
     */
    private void formatUrl(){
        String initialUrl = baseUrl.getText();
        if(initialUrl.endsWith("/")){
            baseUrl.setText(initialUrl.substring(0, initialUrl.length() - 1));
        }
    }
}