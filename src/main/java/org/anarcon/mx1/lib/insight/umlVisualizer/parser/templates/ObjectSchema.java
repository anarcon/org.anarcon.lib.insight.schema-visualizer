package org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates;

@SuppressWarnings("unused")
public class ObjectSchema {

    private final int id;
    private final String name;
    private final String objectCount;
    private final String objectSchemaKey;

    public ObjectSchema(int id, String name, String objectCount, String objectSchemaKey){
        this.id = id;
        this.name = name;
        this.objectCount = objectCount;
        this.objectSchemaKey = objectSchemaKey;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getObjectCount() {
        return objectCount;
    }

    public String getObjectSchemaKey() {
        return objectSchemaKey;
    }

}
