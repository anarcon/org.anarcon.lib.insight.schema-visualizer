package org.anarcon.mx1.lib.insight.umlVisualizer.parser;

import org.anarcon.mx1.lib.insight.umlVisualizer.dialogs.SchemaChoice;
import org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates.InsightObjectType;
import org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates.InsightObjectTypeAttribute;
import org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates.ObjectSchema;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.NodeAndEdges;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.UmlEdgeCreator;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.UmlNodeCreator;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlEdge;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlNode;

import java.util.ArrayList;
import java.util.List;

public class InsightUmlRepository {

    private InsightCaller caller;
    private List<UmlNode> nodes = new ArrayList<>();
    private List<UmlEdge> inheritanceEdges = new ArrayList<>();
    private List<UmlEdge> attributeEdges = new ArrayList<>();
    private List<UmlEdge> parentEdges = new ArrayList<>();

    public InsightUmlRepository(InsightCaller caller) {this.caller = caller;}

    public List<UmlNode> getNodes() { return nodes; }

    public List<UmlEdge> getInheritanceEdges() { return inheritanceEdges; }

    public List<UmlEdge> getAttributeEdges() { return attributeEdges; }

    public List<UmlEdge> getParentEdges() { return parentEdges; }

    /**
     * get a list of schemas that exist in insight, then show a dialog letting the user choose which the UML-diagram
     * should be created from
     * @return the ID
     */
    public int getSchemaId(){
        List<ObjectSchema> schemas = caller.getObjectSchemas();
        //TODO: if null -> u did some login mistakes -> no access -> show dialog :)
        if(schemas.size() > 0){
            SchemaChoice choice = new SchemaChoice("Choose a schema", schemas);
            return choice.showDialog();
        }
        else {
            return -1;
        }
    }

    /**
     * Get all object types in the schema
     * @param objectSchema identifies the schema that the UML diagram is created fromm
     */
    public List<InsightObjectType> collectObjectTypes(int objectSchema){
         return caller.getObjectTypesFromSchema(objectSchema);
    }

    /**
     * create a bunch of nodes and edges that contain all information the diagram is built from
     * @param insightObjectTypes list of all object types, that will be included in the diagram
     */
    public void createUmlShapes(List<InsightObjectType> insightObjectTypes){
        insightObjectTypes.forEach(o -> {
            List<InsightObjectTypeAttribute> attributes =  collectAttributes(o.getId());
            NodeAndEdges objectNodeAndAttributeEdges = UmlNodeCreator.createNode(o, attributes);
            nodes.add(objectNodeAndAttributeEdges.getNode());
            attributeEdges.addAll(objectNodeAndAttributeEdges.getEdges());
            if(o.isInherited()) {
                inheritanceEdges.add(UmlEdgeCreator.createInheritanceEdge(o.getId(), o.getParentObjectTypeId()));
            }
            else{
                if(o.getParentObjectTypeId() != 0) {
                    parentEdges.add(UmlEdgeCreator.createParentageEdge(o.getId(), o.getParentObjectTypeId()));
                }
            }
        });
        System.out.println("Success!");
    }

    /**
     * Returns a list containing all attributes of the object type associated with the id
     * @param objectId identifies the object whose attributes are being collected
     * @return a list of all attributes the object has
     */
    private List<InsightObjectTypeAttribute> collectAttributes(int objectId){
        return caller.getAttributesFromObjectType(objectId);
    }

}
