package org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes;

import java.util.List;


@SuppressWarnings("unused")
public class UmlFunction {
    private final AccessModifier accessModifier;
    private final String name;
    private final String returnType;
    private final List<UmlFunctionParameter> parameters;

    public UmlFunction(AccessModifier accessModifier, String name, String returnType, List<UmlFunctionParameter> parameters) {
        this.accessModifier = accessModifier;
        this.name = name;
        this.returnType = returnType;
        this.parameters = parameters;
    }

    public List<UmlFunctionParameter> getParameters() {
        return parameters;
    }

    public AccessModifier getAccessModifier() {
        return accessModifier;
    }

    public String getName() {
        return name;
    }

    public String getReturnType() {
        return returnType;
    }

    public String functionToString() {
        StringBuilder s = new StringBuilder();
        switch (accessModifier) {
            case PUBLIC:
                s = new StringBuilder("+");
                break;
            case PRIVATE:
                s = new StringBuilder("-");
                break;
            case PROTECTED:
                s = new StringBuilder("#");
                break;
        }
        s.append(name).append("(");
        if (parameters != null)
            for (UmlFunctionParameter p : parameters) {
                s.append(p.functionParameterToString());
            }
        s.append("):");
        return s + returnType;
    }
}
