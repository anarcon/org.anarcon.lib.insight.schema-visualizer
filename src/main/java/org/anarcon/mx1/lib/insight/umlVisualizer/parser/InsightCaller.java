package org.anarcon.mx1.lib.insight.umlVisualizer.parser;

import feign.*;
import org.anarcon.mx1.lib.insight.umlVisualizer.dialogs.GenericMessage;
import org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates.InsightObjectTypeAttribute;
import org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates.ObjectSchema;
import org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates.ObjectSchemaList;
import org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates.InsightObjectType;
import feign.auth.BasicAuthRequestInterceptor;
import feign.gson.GsonDecoder;

import java.util.List;

public class InsightCaller {

    private final JsonClient caller;

    public InsightCaller(String baseUrl, String username, char[] password){
        this.caller = Feign.builder()
                .decoder(new GsonDecoder())
                .requestInterceptor(new BasicAuthRequestInterceptor(username, String.valueOf(password)))
                .target(JsonClient.class, baseUrl);
    }

    public List<ObjectSchema> getObjectSchemas(){
        try {
            return caller.findAllObjectSchemas().getObjectSchemas();
        }
        catch (Exception e){
            handleConnectionException(e);
            return null;
        }
    }

    public List<InsightObjectType> getObjectTypesFromSchema(int objectSchemaId){
        try {
            return caller.findObjectTypesFromSchema(objectSchemaId);
        }
        catch (Exception e){
            handleConnectionException(e);
            return null;
        }
    }

    public List<InsightObjectTypeAttribute> getAttributesFromObjectType(int objectTypeId){
        try {
            return caller.findAttributesFromObjectType(objectTypeId);
        }
        catch (Exception e){
            handleConnectionException(e);
            return null;
        }
    }

    private void handleConnectionException(Exception e) {
        GenericMessage warning;
        if (e instanceof RetryableException) {
            warning = new GenericMessage("Connection to server not possible, please make sure " +
                    "URL is correct!", "Connection not possible");
        } else {
            if (e instanceof FeignException) {
                FeignException exc = (FeignException) e;
                switch (exc.status()) {
                    case 401:
                        warning = new GenericMessage("Not allowed. Please make sure username and password is " +
                                "correct. Note that you may be required to enter a captcha in your browser if you tried logging " +
                                "in with an incorrect password too often.", "Error 401");
                        break;
                    case 404:
                        warning = new GenericMessage("Connection not possible, is Insight installed?",
                                "Error 404");
                        break;
                    default:
                        warning = new GenericMessage(exc.getMessage(), "Error " + exc.status());
                }
            } else {
                warning = new GenericMessage(e.getMessage(), "Error");
            }
        }
        warning.showDialog();
    }
}

interface JsonClient {
    @RequestLine("GET /rest/insight/1.0/objectschema/list")
    ObjectSchemaList findAllObjectSchemas();

    @RequestLine("GET /rest/insight/1.0/objectschema/{objectSchemaId}/objecttypes/flat")
    List<InsightObjectType> findObjectTypesFromSchema(@Param("objectSchemaId") int objectSchemaId);

    @RequestLine("GET /rest/insight/1.0/objecttype/{attributeId}/attributes")
    List<InsightObjectTypeAttribute> findAttributesFromObjectType(@Param("attributeId") int attributeId);
}
