package org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes;

public enum UmlArrow {
    INHERITANCE,
    PARENTAGE,
    NOARROW
}
