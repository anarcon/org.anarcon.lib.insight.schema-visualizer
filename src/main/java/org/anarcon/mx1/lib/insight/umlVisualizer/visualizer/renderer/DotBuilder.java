package org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.renderer;

import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class builds a .dot file / String
 */

public class DotBuilder {
    private Map<Integer, UmlNode> umlNodes;
    private List<UmlEdge> umlEdges;

    public DotBuilder(Map<Integer, UmlNode> umlNodes, List<UmlEdge> umlEdges) {
        this.umlNodes = umlNodes;
        this.umlEdges = umlEdges;
    }


    private String createInheritanceSubgraphs() {
        Map<Integer, String> subgraphs = new HashMap<>();
        ArrayList<UmlEdge> edges = new ArrayList<>(umlEdges);
        for (UmlEdge e : edges) {
            if (!(umlNodes.get(e.getStart()) == null || umlNodes.get(e.getEnd()) == null)) {
                if (e.getArrow().equals(UmlArrow.INHERITANCE)) {
                    if (subgraphs.get(e.getEnd()) == null) {
                        subgraphs.put(e.getEnd(),
                                "subgraph cluster_inherited_by_" + umlNodes.get(e.getEnd()).getName().replace(' ', '_')
                                        + " {\n" + "\"" + umlNodes.get(e.getStart()).getName() + "\"" + " }");
                    } else {
                        String s = subgraphs.get(e.getEnd());
                        s = s.substring(0, s.length() - 1);
                        s += "," + "\"" + umlNodes.get(e.getStart()).getName() + "\"" + " }";
                        subgraphs.put(e.getEnd(), s);
                        umlEdges.remove(e);
                    }

                }
            }
        }
        StringBuilder s = new StringBuilder();
        for (String subString : subgraphs.values()) {
            s.append(subString);
        }
        return s.toString();
    }

    /**
     * Creates the String to a node
     */
    private String node(UmlNode node) {
        String s = "";
        s += "\"" + node.getName() + "\"";
        s += "[ \"shape\"=\"none\"";
        //here comes the node body
        s += "\"label\"=<";
        //here comes the html table
        s += "<table border=\"0\" cellborder=\"1\" cellspacing=\"0\">";
        s += classnameToHtml(node);
        s += attributesToHtml(node);
        s += functionsToHtml(node);
        s += "</table>";
        s += ">]\n";
        return s;
    }

    /**
     * Creates the String to an edge
     */
    private String edge(UmlEdge edge) {
        //Catch weird edges...
        if (umlNodes.get(edge.getStart()) == null) {
            System.out.println("could not find node with id: " + edge.getStart());
            return "";
        }
        if (umlNodes.get(edge.getEnd()) == null) {
            System.out.println("could not find node with id: " + edge.getEnd());
            return "";
        }
        UmlNode start = umlNodes.get(edge.getStart());
        UmlNode end = umlNodes.get(edge.getEnd());
        String s = "";
        //build a node in between in case there is a reference type
        if (edge.getReferenceTypeName() != null) {
            //create the Label node name
            String label = "Label" + "_" + start.getName() + "_" + end.getName() + "_" + edge.getReferenceTypeName();
            //create the label node
            s += "\"" + label + "\" [\"label\"=<" + edge.getReferenceTypeName() + ">]\n";
            //start ->label
            s += "\"" + start.getName() + "\"";
            s += "->";
            s += "\"" + label + "\"\n";
            s += "[";
            s += "\"arrowhead\"=\"none\"";
            if (edge.getStartCardinality() != null) s += "\"taillabel\"=\"" + edge.getStartCardinality() + "\"";
            s += "]\n";
            //label->end
            s += "\"" + label + "\"";
            s += "->";
            s += "\"" + end.getName() + "\"";
            s += "[";
            if (edge.getEndCardinality() != null) s += "\"headlabel\"=\"" + edge.getEndCardinality() + "\"";
        }
        //simple edge with no label
        else {
            //start node
            s += "\"" + start.getName() + "\"";
            //ref
            s += "->";
            //end node
            s += "\"" + end.getName() + "\"";
            //here come the edge attributes
            s += "[";
            //cardinality
            if (edge.getEndCardinality() != null) s += "\"headlabel\"=\"" + edge.getEndCardinality() + "\"";
            if (edge.getStartCardinality() != null) s += "\"taillabel\"=\"" + edge.getStartCardinality() + "\"";
        }
        //arrowheadType
        switch (edge.getArrow()) {
            case NOARROW:
                s += "\"arrowhead\"=\"none\"";
                break;
            case PARENTAGE:
                s += "\"arrowhead\"=\"odiamond\""; //TODO: -> what should Parentage look like
                break;
            case INHERITANCE:
                s += "\"arrowhead\"=\"onormal\" , arrowsize=1.5";
                s += ", ltail=" + "cluster_inherited_by_" + umlNodes.get(edge.getEnd()).getName().replace(' ', '_');
                break;
        }
        s += "]\n";
        return s;
    }

    /**
     * Creates a String that can be rendered to a graph via f.e. graphiz online
     */
    private String graph() {
        StringBuilder s = new StringBuilder("digraph{\ncompound=true\n");
        s.append("graph [splines=false, rankdir=\"BT\"" + "]\n ");
        s.append(createInheritanceSubgraphs());
        for (UmlNode n : umlNodes.values()) {
            s.append(node(n));
        }
        for (UmlEdge e : umlEdges) {
            s.append(edge(e));
        }
        return s + "}";
    }

    /**
     * Next three methods create the html for the columns of the table of a UML node
     */
    private String classnameToHtml(UmlNode node) {
        String s = "";
        if (node.getName() != null) {
            s += "<tr>\n" + "<td>";
            if (node.isAbstract()) {
                s += "&lt;&lt;abstract&gt;&gt;<br/>\n";
            }
            s += node.getName() + "</td>\n" + "</tr>";
        }
        return s;
    }

    private String attributesToHtml(UmlNode node) {
        StringBuilder s = new StringBuilder();
        if (node.getAttributes() != null) {
            s.append("<tr>\n" + "<td align=\"left\">");
            for (UmlAttribute a : node.getAttributes()) {
                s.append(a.attributeToString()).append("<br/>");
            }
            s.append("</td>" + "</tr>");
        }
        return s.toString();
    }

    private String functionsToHtml(UmlNode node) {
        StringBuilder s = new StringBuilder();
        if (node.getFunctions() != null) {
            s.append("<tr>\n" + "<td align=\"left\">");
            for (UmlFunction f : node.getFunctions()) {
                s.append(f.functionToString()).append("<br/>");
            }
            s.append("</td>" + "</tr>");
        }
        return s.toString();
    }

    /**
     * Creates a .dot witch can be found in the files
     */
    public void dotGraph() throws IOException {
        String s = graph();
        System.out.println(s);
        File file = new File("/home/korbi/graph.dot");
        //Create the file
        if (file.createNewFile()) {
            System.out.println("File is created!");
        } else {
            System.out.println("File already exists.");
        }
        //Write Content
        FileWriter writer = new FileWriter(file);
        writer.write(s);
        writer.close();
    }

    /**
     * Creates a .dot and a .png witch can be found in the files folder
     */
    @SuppressWarnings("unused")
    public void pngGraph() {
        //TODO: do we want this?
    }


    /**
     * Tests the String builder -> kept this for building test later...
     */
    public static void main(String[] args) {

        ArrayList<UmlAttribute> attributes1 = new ArrayList<>();
        ArrayList<UmlAttribute> attributes2 = new ArrayList<>();

        UmlNode node1 = new UmlNode("node1", 1, attributes1, null, false);
        UmlNode node2 = new UmlNode("node2", 2, attributes1, null, false);
        UmlNode node3 = new UmlNode("node3", 3, attributes2, null, false);
        UmlNode node4 = new UmlNode("node4", 4, attributes1, null, true);
        UmlNode node5 = new UmlNode("node5", 5, attributes1, null, false);


        UmlEdge edge1 = new UmlEdge(2, 1, UmlArrow.INHERITANCE, null, null, null);
        UmlEdge edge2 = new UmlEdge(3, 2, UmlArrow.INHERITANCE, null, null, null);
        UmlEdge edge3 = new UmlEdge(2, 4, UmlArrow.NOARROW, "1", "*", "hallo");
        UmlEdge edge4 = new UmlEdge(5, 3, UmlArrow.NOARROW, "(1,3)", "(1,*)", "peter");

        Map<Integer, UmlNode> nodes = new HashMap<>();
        nodes.put(node1.getId(), node1);
        nodes.put(node2.getId(), node2);
        nodes.put(node3.getId(), node3);
        nodes.put(node4.getId(), node4);
        nodes.put(node5.getId(), node5);


        List<UmlEdge> edges = new ArrayList<>();
        edges.add(edge1);
        edges.add(edge2);
        edges.add(edge3);
        edges.add(edge4);

        DotBuilder s = new DotBuilder(nodes, edges);
        try {
            s.dotGraph();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
