package org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes;

import java.util.List;

public class UmlNode {
    private final String name;
    private final int id;
    private final List<UmlAttribute> attributes;
    private List<UmlFunction> functions;
    private final boolean isAbstract;

    public UmlNode(String name, int id, List<UmlAttribute> attributes, List<UmlFunction> functions, boolean isAbstract) {
        this(name, id, attributes, isAbstract);
        this.functions = functions;
    }

    public UmlNode(String name, int id, List<UmlAttribute> attributes, boolean isAbstract){
        this.name = name;
        this.id = id;
        this.attributes = attributes;
        this.isAbstract = isAbstract;
    }

    public String getName() { return name; }

    public int getId() { return id; }

    public List<UmlAttribute> getAttributes() { return attributes; }

    public List<UmlFunction> getFunctions() { return functions; }

    public boolean isAbstract() { return isAbstract; }
}
