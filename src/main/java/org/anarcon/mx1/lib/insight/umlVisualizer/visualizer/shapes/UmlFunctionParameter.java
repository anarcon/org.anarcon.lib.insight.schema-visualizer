package org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes;

@SuppressWarnings("unused")
public class UmlFunctionParameter {
    private final String name;
    private final String type;

    public UmlFunctionParameter(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String functionParameterToString(){
        return name + ":" + type;
    }
}

