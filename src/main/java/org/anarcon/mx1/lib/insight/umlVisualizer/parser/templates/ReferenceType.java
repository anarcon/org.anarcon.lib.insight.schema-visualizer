package org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates;

public class ReferenceType {
    private final String name;

    public ReferenceType(String name) {
        this.name = name;
    }

    public String getName() { return name; }

}
