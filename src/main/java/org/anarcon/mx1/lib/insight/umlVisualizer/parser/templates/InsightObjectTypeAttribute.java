package org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates;

@SuppressWarnings("unused")
public class InsightObjectTypeAttribute {

    private final int id;
    private final String name;

    /** What type is this attribute? 0 is "default" data types, 1-7 are links to other entities
     * For details see: https://documentation.riada.se/display/ICV53/Object+Type+Attributes+-+REST
     */
    private final int type;
    private final InsightObjectType objectType;
    private final DefaultType defaultType;
    private final Integer referenceObjectTypeId;
    private final ReferenceType referenceType;
    private final int minimumCardinality;
    private final int maximumCardinality;

    public InsightObjectTypeAttribute(int id, String name, int type, InsightObjectType objectType,
                                      DefaultType defaultType, Integer referenceObjectTypeId,
                                      ReferenceType referenceType, int minimumCardinality, int maximumCardinality) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.objectType = objectType;
        this.defaultType = defaultType;
        this.referenceObjectTypeId = referenceObjectTypeId;
        this.referenceType = referenceType;
        this.minimumCardinality = minimumCardinality;
        this.maximumCardinality = maximumCardinality;
    }

    public int getId() { return id; }

    public String getName() { return name; }

    public int getType() { return type; }

    public InsightObjectType getObjectType() { return objectType; }

    public DefaultType getDefaultType() { return defaultType; }

    public Integer getReferenceObjectTypeId() { return referenceObjectTypeId; }

    public ReferenceType getReferenceType() { return referenceType; }

    public int getMinimumCardinality() { return minimumCardinality; }

    public int getMaximumCardinality() { return maximumCardinality; }

    /**
     * create cardinality from maximumCardinality and minimumCardinality that can be shown in a UML diagram
     * @return String representation of cardinality in (min, max) notation
     */
    public String getCardinality(){
        String maxCard = "";
        if(maximumCardinality == -1){
            maxCard = "*";
        }
        else {
            maxCard += maximumCardinality;
        }
        return "("+minimumCardinality+", "+maxCard+")";
    }
}
