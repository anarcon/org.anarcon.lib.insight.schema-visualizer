package org.anarcon.mx1.lib.insight.umlVisualizer.visualizer;

import org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates.InsightObjectType;
import org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates.InsightObjectTypeAttribute;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlAttribute;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlEdge;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlNode;

import java.util.ArrayList;
import java.util.List;

public class UmlNodeCreator {

    /**
     * create a new node from an object type in insight
     * @param objectType the object type that the node will be created for
     * @param objectTypeAttributes attributes of that object type
     * @return an object containing a list of edges and a node
     */
    public static NodeAndEdges createNode(InsightObjectType objectType,
                                          List<InsightObjectTypeAttribute> objectTypeAttributes){

        AttributesAndEdges attributesAndEdges = createAttributeList(objectTypeAttributes, objectType.getId());

        return new NodeAndEdges(new UmlNode(objectType.getName(), objectType.getId(),
                attributesAndEdges.getAttributes(), objectType.isAbstractObjectType()), attributesAndEdges.getEdges());
    }

    /**
     * Create Edges and uml attributes for a certain object type
     * @param insightAttributes attributes as they are shown in insight
     * @param objectTypeId object type id these attributes and edges belong to
     * @return an object containing two lists, one populated with attributes and one with edges
     */
    private static AttributesAndEdges createAttributeList(List<InsightObjectTypeAttribute> insightAttributes,
                                                                              int objectTypeId){
        List<UmlAttribute> umlAttributes = new ArrayList<>();
        List<UmlEdge> umlEdges = new ArrayList<>();
        insightAttributes.forEach(o ->{
            /* Makes sure attribute is not inherited **/
            if(o.getObjectType().getId() == objectTypeId) {
                switch (o.getType()) {
                    /* One of the default types */
                    case 0: {
                        umlAttributes.add(UmlAttribute.createDefaultAttribute(o.getName(), o.getDefaultType().getId()));
                        break;
                    }
                    /* References other insight objects */
                    case 1: {
                        umlAttributes.add(UmlAttribute.createObjectReferenceAttribute(o.getName(),
                                o.getReferenceObjectTypeId(), o.getReferenceType().getName()));
                        umlEdges.add(UmlEdgeCreator.createAttributeEdge(objectTypeId, o.getReferenceObjectTypeId(),
                                o.getReferenceType().getName(), o.getCardinality()));
                        break;
                    }

                    /* References objects not in insight */
                    default: {
                        umlAttributes.add(UmlAttribute.createNonInsightReferenceAttribute(o.getName(), o.getType()));
                        break;
                    }

                }
            }
        });
        return new AttributesAndEdges(umlAttributes, umlEdges);
    }
}
