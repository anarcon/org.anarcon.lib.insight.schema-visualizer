package org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.renderer;

import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlArrow;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlAttribute;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlEdge;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Filter {
    /**
     * This Filter can be used to do certain adjustments to the UML diagram
     * Features so far:
     *      - delete a node and it's inherited children, map removed edges to Attributes (Filter 1)
     *
     *@param umlNodes = the nodes of the graph
     *@param umlEdges = the edges of the graph
     */


    /**Filter methods*/

    /**
     * (Filter 1)
     * removes the children of a node and maps their edges to attributes in the other nodes
     * @param nodeName = the name of the node whose children are supposed to be removed
     */
    public static void removeNodes(String nodeName, Map<Integer, UmlNode> umlNodes, List<UmlEdge> umlEdges){
        List<UmlNode> children = getChildren(nodeName, umlNodes, umlEdges);
        concatNodesAndEdges(children, umlNodes, umlEdges);
        removeNodes(children, umlNodes);
    }


    /**Private methods*/

    /**
     * Removes the nodes in the list -> replaces noarrow edges with attributes
     * Adds the cardinality edges as Attribute to the other nodes when removed
     * @param nodes = the list of nodes that will be removed
     */
    private static  void removeNodes(List<UmlNode> nodes, Map<Integer, UmlNode> umlNodes) {
        nodes.forEach(n ->{
            umlNodes.remove(n.getId());
            System.out.println("removed " + n.getName());
        });
    }

    /**
     * concatenates the related nodes of a list of nodes which are supposed to be removed to keep the attribute edges
     * @param nodes = the nodes that will be removed
     */
    private static void concatNodesAndEdges(List<UmlNode> nodes, Map<Integer, UmlNode> umlNodes, List<UmlEdge> umlEdges){
        umlEdges.forEach(e -> {
            if(nodes.contains(umlNodes.get(e.getEnd()))&&e.getArrow().equals(UmlArrow.NOARROW)){
                umlNodes.put(e.getStart(), concatNodeAndEdge(umlNodes.get(e.getStart()), e));
            }
            if(nodes.contains(umlNodes.get(e.getStart()))&&e.getArrow().equals(UmlArrow.NOARROW)){
                umlNodes.put(e.getEnd(), concatNodeAndEdge(umlNodes.get(e.getEnd()), e));
            }
        });
    }

    /**
     * @param node = the old node with the given attributes
     * @param edge = contains the now attributes
     * @return a new UmlNode with the additional attribute
     */
    private static UmlNode concatNodeAndEdge(UmlNode node, UmlEdge edge){
        if(node.getId()==edge.getStart()){
            List<UmlAttribute> attributes = new ArrayList<>(node.getAttributes());
            attributes.add(UmlAttribute.createDefaultAttribute("newATStart", 0));
            return new UmlNode(node.getName(),node.getId(), attributes,node.getFunctions(),node.isAbstract());
        }
        else if(node.getId()==edge.getEnd()){
            List<UmlAttribute> attributes = new ArrayList<>(node.getAttributes());
            attributes.add(UmlAttribute.createDefaultAttribute("newATEnd", 0));
            return new UmlNode(node.getName(),node.getId(),attributes,node.getFunctions(),node.isAbstract());
        }
        else {
            System.err.println("concat makes no sense!");
            return node;
        }
    }

    /**Gets all the children of a node
     * @param nodeName = the name of the node whose children are wanted
     * */
    private static List<UmlNode> getChildren(String nodeName, Map<Integer, UmlNode> umlNodes, List<UmlEdge> umlEdges){
        List<List<UmlNode>> childrenLists = new ArrayList<>();
        List<UmlNode> children = new ArrayList<>();
        umlEdges.forEach(e -> {
            if(umlNodes.get(e.getEnd())!=null) {
                if (umlNodes.get(e.getEnd()).getName().equals(nodeName) && e.getArrow().equals(UmlArrow.INHERITANCE)) {
                    children.add(umlNodes.get(e.getStart()));
                    if(!children.contains(umlNodes.get(e.getEnd()))){
                        children.add(umlNodes.get(e.getEnd()));
                    }
                }
            }
        });
        children.forEach(n -> {
            if(!n.getName().equals(nodeName))
                childrenLists.add(getChildren(n.getName(), umlNodes, umlEdges));
        });
        childrenLists.forEach(cl -> cl.forEach(c -> {
            if(!children.contains(c)){
                children.add(c);
            }
        }));
        for (UmlNode n: children) {
            System.out.println("found children: " + n.getName());
        }
        return children;
    }

}
