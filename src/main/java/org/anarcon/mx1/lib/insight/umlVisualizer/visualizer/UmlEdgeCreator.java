package org.anarcon.mx1.lib.insight.umlVisualizer.visualizer;

import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlArrow;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlEdge;

import java.util.Optional;

public class UmlEdgeCreator {
    private UmlEdgeCreator(){}

    /**
     * Create a new UmlEdge expressing a connection between two objects where one inherits the other.
     * @param startNode ID of the starting node
     * @param endNode ID of the end node
     * @return the new edge
     */
    public static UmlEdge createInheritanceEdge(int startNode, int endNode){
        return new UmlEdge(startNode, endNode, UmlArrow.INHERITANCE, null, null,
                null);
    }

    /**
     * Create a new UmlEdge expressing a connection between two objects that reference one another.
     * @param startNode ID of the starting node
     * @param endNode ID of the end node
     * @param referenceTypeName Name of the reference
     * @return the new edge
     */
    public static UmlEdge createAttributeEdge(int startNode, int endNode, String referenceTypeName, String startMultiplicity){
        return new UmlEdge(startNode, endNode, UmlArrow.NOARROW, startMultiplicity, "*",
                referenceTypeName);
    }

    /**
     * Create a new UmlEdge expressing a connection between two objects where one is the others parent, without
     * any inheritance
     * @param startNode ID of the starting node
     * @param endNode ID of the end node
     * @return the new edge
     */
    public static UmlEdge createParentageEdge(int startNode, Integer endNode){
        return new UmlEdge(startNode, endNode, UmlArrow.PARENTAGE, null, null,
                null);
    }
}
