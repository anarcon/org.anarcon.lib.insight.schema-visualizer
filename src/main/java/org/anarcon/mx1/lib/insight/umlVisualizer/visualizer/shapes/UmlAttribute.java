package org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes;

@SuppressWarnings("unused")
public class UmlAttribute {

    private final String name;
    /** List 0-7, List here: https://documentation.riada.se/display/ICV53/Object+Type+Attributes+-+REST */
    private final int type;

    /** Next two attributes are only used if the UmlAttribute is a reference to another object */
    private final Integer referenceObjectTypeId;
    private final String referenceName;

    /** Used only if type == 0, defines the kind of data(Text, int, bool etc.) this attribute contains */
    private final int defaultType;
    private final AccessModifier accessModifier;

    private UmlAttribute(String name, int type, Integer referenceObjectTypeId, String referenceName, int defaultType,
                         AccessModifier accessModifier) {
        this.name = name;
        this.type = type;
        this.referenceObjectTypeId = referenceObjectTypeId;
        this.referenceName = referenceName;
        this.defaultType = defaultType;
        this.accessModifier = accessModifier;
    }

    /**
     * Constructor for attributes that are one of the default types
     * @param name name of the attribute
     * @param defaultType default type, 0-10
     */
    public static UmlAttribute createDefaultAttribute(String name, int defaultType) {
        return new UmlAttribute(name, 0, null, null ,defaultType , AccessModifier.PUBLIC);
    }

    /**
     * Constructor for attributes that are references to other insight objects
     * @param name name of the attribute
     * @param referenceObjectTypeId id of the object referenced
     * @param referenceName name of the reference to the other object
     */
    public static UmlAttribute createObjectReferenceAttribute(String name, Integer referenceObjectTypeId,
                                                              String referenceName) {
        return new UmlAttribute(name, 1, referenceObjectTypeId, referenceName,1, AccessModifier.PUBLIC);
    }

    /**
     * Constructor for attributes, that are references to non-insight objects
     * @param name name of the attribute
     * @param type type of the attribute (2-7 if this constructor is called)
     */
    public static UmlAttribute createNonInsightReferenceAttribute(String name, int type){
        return new UmlAttribute(name, type, null, null,0,
                AccessModifier.PUBLIC);
    }

    public String getName() { return name; }

    public int getType() {
        return type;
    }

    public int getDefaultType() {
        return defaultType;
    }

    public AccessModifier getAccessModifier() { return accessModifier; }

    public Integer getReferenceObjectTypeId() { return referenceObjectTypeId; }

    public String getReferenceName() { return referenceName; }

    public String attributeToString(){
        String s = "";
        switch (accessModifier){
            case PUBLIC: s = "+";
            break;
            case PRIVATE: s ="-";
            break;
            case PROTECTED: s="#";
            break;
        }
        return s + name + ":" + type;
    }
}

