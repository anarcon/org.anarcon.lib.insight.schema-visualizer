package org.anarcon.mx1.lib.insight.umlVisualizer.visualizer;

import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlEdge;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlNode;

import java.util.List;

/**
 * Helper class to return one node and a list of edges
 */
public class NodeAndEdges {

    private final UmlNode node;
    private final List<UmlEdge> edges;

    NodeAndEdges(UmlNode node, List<UmlEdge> edges) {
        this.node = node;
        this.edges = edges;
    }

    public UmlNode getNode() { return node; }

    public List<UmlEdge> getEdges() { return edges; }

}
