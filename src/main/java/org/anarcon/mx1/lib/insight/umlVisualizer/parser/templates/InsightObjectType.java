package org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates;

public class InsightObjectType {

    private final int id;
    private final String name;
    private final boolean inherited;
    private final int parentObjectTypeId;
    private final boolean abstractObjectType;

    public InsightObjectType(int id, String name, boolean inherited, int parentObjectTypeId, boolean abstractObjectType) {
        this.id = id;
        this.name = name;
        this.inherited = inherited;
        this.parentObjectTypeId = parentObjectTypeId;
        this.abstractObjectType = abstractObjectType;
    }

    public int getId() { return id; }

    public String getName() { return name; }

    public boolean isInherited() { return inherited; }

    public int getParentObjectTypeId() { return parentObjectTypeId; }

    public boolean isAbstractObjectType() { return abstractObjectType; }

}
