package org.anarcon.mx1.lib.insight.umlVisualizer.visualizer;

import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlAttribute;
import org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes.UmlEdge;

import java.util.List;

/**
 * Helper class to return a list of attributes and a list of edges
 */
public class AttributesAndEdges {
    private final List<UmlAttribute> attributes;
    private final List<UmlEdge> edges;

    AttributesAndEdges(List<UmlAttribute> attributes, List<UmlEdge> edges) {
        this.attributes = attributes;
        this.edges = edges;
    }

    public List<UmlAttribute> getAttributes() { return attributes; }

    public List<UmlEdge> getEdges() { return edges; }

}
