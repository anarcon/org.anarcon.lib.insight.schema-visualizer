package org.anarcon.mx1.lib.insight.umlVisualizer.dialogs;

import org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates.ObjectSchema;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class SchemaChoice extends JDialog {

        private final JList<String> list;
        private final JLabel label;
        private JButton okButton, cancelButton;
        private JDialog dialog;
        private final List<ObjectSchema> listToDisplay;
        private int chosenId = -1;

        public SchemaChoice(String message, List<ObjectSchema> listToDisplay){
            this.listToDisplay = listToDisplay;
            list = makeJlistFromObjectSchemas();
            label = new JLabel(message);
            createAndDisplayOptionPane();
        }

        private void createAndDisplayOptionPane(){
            setupButtons();
            JPanel pane = layoutComponents();
            JOptionPane optionPane = new JOptionPane(pane);
            optionPane.setOptions(new Object[]{okButton, cancelButton});
            dialog = optionPane.createDialog("Select option");
        }

        private void setupButtons(){
            okButton = new JButton("Ok");
            okButton.addActionListener(e -> handleOkButtonClick());

            cancelButton = new JButton("Cancel");
            cancelButton.addActionListener(e -> handleCancelButtonClick());
        }

        private JPanel layoutComponents(){
            centerListElements();
            JPanel panel = new JPanel(new BorderLayout(5,5));
            panel.add(label, BorderLayout.NORTH);
            panel.add(list, BorderLayout.CENTER);
            return panel;
        }

        private void centerListElements(){
            DefaultListCellRenderer renderer = (DefaultListCellRenderer) list.getCellRenderer();
            renderer.setHorizontalAlignment(SwingConstants.CENTER);
        }

        private void handleOkButtonClick(){
            String id = list.getSelectedValue();
            chosenId = Integer.parseInt(id.substring(0,id.indexOf(" ")));
            dialog.setVisible(false);
            dialog.dispose();
        }

        private void handleCancelButtonClick(){
            chosenId = -1;
            dialog.setVisible(false);
            dialog.dispose();
        }

        public int showDialog(){
            dialog.setVisible(true);
            return chosenId;
        }

        public Object getSelectedItem(){ return list.getSelectedValue(); }

        private JList<String> makeJlistFromObjectSchemas(){
            String choicesString[] = new String[this.listToDisplay.size()];
            for(int i = 0; i < choicesString.length; i++){
                choicesString[i] = this.listToDisplay.get(i).getId()+" "+this.listToDisplay.get(i).getName();
            }
            return new JList<>(choicesString);
        }
    }

