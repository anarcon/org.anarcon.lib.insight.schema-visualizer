package org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates;

public class DefaultType {
    private final int id;
    private final String name;

    public DefaultType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() { return id; }

    public String getName() { return name; }
}
