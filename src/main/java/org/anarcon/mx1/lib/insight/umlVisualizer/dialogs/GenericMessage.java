package org.anarcon.mx1.lib.insight.umlVisualizer.dialogs;

import javax.swing.*;
import java.awt.*;

/**
 * Can be used to create a generic dialog offering to accept or cancel and returning true or false
 */
public class GenericMessage extends JDialog {

    private JButton okButton;
    private final JDialog dialog;

    public GenericMessage(String message, String title){
        setupButtons();
        JPanel panel = new JPanel(new BorderLayout(5, 5));
        JLabel label = new JLabel(message);
        panel.add(label, BorderLayout.CENTER);
        JOptionPane optionPane = new JOptionPane(panel);
        optionPane.setOptions(new Object[]{okButton});
        dialog = optionPane.createDialog(title);
    }

    private void setupButtons(){
        okButton = new JButton("Ok");

        okButton.addActionListener(e -> handleButton());
    }

    private void handleButton(){
        dialog.setVisible(false);
        dialog.dispose();
    }

    public void showDialog(){
        dialog.setVisible(true);
    }
}
