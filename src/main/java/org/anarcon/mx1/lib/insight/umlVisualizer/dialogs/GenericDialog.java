package org.anarcon.mx1.lib.insight.umlVisualizer.dialogs;

import javax.swing.*;
import java.awt.*;

/**
 * Can be used to create a generic dialog offering to accept or cancel and returning true or false
 */
public class GenericDialog extends JDialog {

    private JButton okButton, cancelButton;
    private final JDialog dialog;
    private boolean decision;

    public GenericDialog(String message, String title){
        setupButtons();
        JPanel panel = new JPanel(new BorderLayout(5, 5));
        JLabel label = new JLabel(message);
        panel.add(label, BorderLayout.CENTER);
        JOptionPane optionPane = new JOptionPane(panel);
        optionPane.setOptions(new Object[]{okButton, cancelButton});
        dialog = optionPane.createDialog(title);
    }

    private void setupButtons(){
        okButton = new JButton("Ok");
        cancelButton = new JButton("Cancel");

        okButton.addActionListener(e -> handleButton(true));
        cancelButton.addActionListener(e -> handleButton(false));
    }

    private void handleButton(boolean decision){
        this.decision = decision;
        dialog.setVisible(false);
        dialog.dispose();
    }

    public boolean showDialog(){
        dialog.setVisible(true);
        return decision;
    }
}
