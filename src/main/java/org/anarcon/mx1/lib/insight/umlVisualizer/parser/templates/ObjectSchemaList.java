package org.anarcon.mx1.lib.insight.umlVisualizer.parser.templates;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ObjectSchemaList {

    @SerializedName("objectschemas")
    private final List<ObjectSchema> objectSchemas;

    public ObjectSchemaList(List<ObjectSchema> objectSchemas) {
        this.objectSchemas = objectSchemas;
    }

    public List<ObjectSchema> getObjectSchemas() {
        return objectSchemas;
    }

}
