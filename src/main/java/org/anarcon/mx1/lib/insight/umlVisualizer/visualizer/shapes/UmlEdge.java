package org.anarcon.mx1.lib.insight.umlVisualizer.visualizer.shapes;

public class UmlEdge {

    private final int start;
    private final int end;
    private final UmlArrow arrow;
    private final String startCardinality;
    private final String endCardinality;
    private final String referenceTypeName;

    public UmlEdge(int start, int end, UmlArrow arrow, String startCardinality, String endCardinality, String referenceTypeName) {
        this.start = start;
        this.end = end;
        this.arrow = arrow;
        this.startCardinality = startCardinality;
        this.endCardinality = endCardinality;
        this.referenceTypeName = referenceTypeName;
    }

    public int getStart() { return start; }

    public int getEnd() { return end; }

    public UmlArrow getArrow() { return arrow; }

    public String getStartCardinality() { return startCardinality; }

    public String getEndCardinality() { return endCardinality; }

    public String getReferenceTypeName() { return referenceTypeName; }

}
