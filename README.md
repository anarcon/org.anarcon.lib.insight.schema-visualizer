# MX1 UML Visualizer

A java library that parses .json and renders UML diagrams

**Rendering Modes:**
- .dot
   
## TODO
- types of attributes -> what does 1, 2, 3... stand for (only MX1)
- create the MX1 UML diagram and upload it -> types are missing
- write tests
- TODO in InsightUmlRepository
- discuss how to display parentage in UML diagram (difference to
    inheritance) -> add in DotBuilder (now diamond)
- maybe add other output options like jpg/png etc.
- Integrate renderer with data access
- Add filter options in the GUI
- Display deleted edges as Attributes (naming in Filter) 